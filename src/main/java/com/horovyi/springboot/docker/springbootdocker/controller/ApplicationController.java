package com.horovyi.springboot.docker.springbootdocker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ihor.horovyi 2019-03-28
 */
@RestController
public class ApplicationController {

    @GetMapping("/")
    public String index() {
        return "{'title': 'Hello World'}";
    }

}
